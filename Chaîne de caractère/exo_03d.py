nb_lines, nb_words = [int(x) for x in input().split(" ")]

nb_letters = [0] * 101
for i in range(nb_lines):
   words = input().split(" ")
   for word in words:
      nb_letters[len(word)] += 1

for i in range(len(nb_letters)):
   if nb_letters[i] != 0:
      print(str(i) + " : " + str(nb_letters[i]))
   